## ---- checkpoint ----

library(checkpoint)
checkpoint("2018-07-24", checkpointLocation = tempdir())


## ---- custom_font ----

cust_font <- list(family = "Equity Caps A",
                  size = 16)


## ---- load_packages ----

library(dplyr)
library(readr)
library(plotly)
library(purrr)


## ---- load_csv ----

data <- read_csv("candy_crush.csv")
head(data)

## ---- check_data ----

#Number of NA player IDs
nNa <- data %>%
  filter(is.na(player_id)) %>%
  summarise(n())

nNa

#Number of unique players
nUnique <- data %>%
  filter(!is.na(player_id)) %>%
  summarise(n = n_distinct(player_id))

nUnique

#Time range
rang <- range(data$dt)

rang

## ---- calc_attempts ----

tot_attempt <- data %>%
  group_by(level) %>%
  summarise(
    tot_attempts = sum(num_attempts),
    tot_players = n_distinct(player_id),
    avg_attempts = tot_attempts / tot_players
  )
tot_attempt

## ---- plot_attempts ----

tot_attempt %>%
  plot_ly(
    x = ~ level,
    y =  ~ avg_attempts,
    type = "bar",
    name = "number of attempts / player"
  ) %>%
  layout(
    title = "attempts per player",
    yaxis = list(title = 'count'),
    xaxis = list(title = 'level'),
    font = cust_font
  )

## ---- calc_pwin ----

diff_by_level <- data %>%
  group_by(level) %>%
  summarise(
    tot_wins = sum(num_success),
    tot_attempts = sum(num_attempts),
    p_win = tot_wins / tot_attempts
  )
diff_by_level


## ---- plot_diffprof ----


diff_by_level %>%
  plot_ly(
    x =  ~ level,
    y =  ~ p_win,
    type = "scatter",
    mode = "lines+markers",
    name = "p_win"
  ) %>%
  layout(
    title = "difficulty profile of episode",
    xaxis = list(title = "level"),
    yaxis = list(title = "probability of winning"),
    font = cust_font
  )

## ---- draw_line ----

diff_by_level %>%
  plot_ly(
    x =  ~ level,
    y =  ~ p_win,
    type = "scatter",
    mode = "lines+markers",
    name = "p_win"
  ) %>%
  add_lines(x = 0:16, y =  ~ 0.1, name = "10%") %>%
  layout(
    title = "difficulty profile of episode",
    xaxis = list(title = "level"),
    yaxis = list(title = "probability of winning"),
    font = cust_font
  )


## ---- calc_error ----


diff_by_level <- diff_by_level %>%
  mutate(error = sqrt(p_win * (1 - p_win) / tot_attempts))

diff_by_level


## ---- plot_uncert ----

diff_by_level %>%
  plot_ly(
    x =  ~ level,
    y =  ~ p_win,
    type = "scatter",
    mode = "lines+markers",
    name = "p_win",
    error_y =  ~ list(value = error)
  ) %>%
  add_lines(x = 0:16, y =  ~ 0.1, error_y = list(visible = F), name = "10%") %>%
  layout(
    title = "difficulty profile of episode",
    xaxis = list(title = "level"),
    yaxis = list(title = "probability of winning"),
    font = cust_font
  )

## ---- prop_win_all ----

p <-  reduce(diff_by_level$p_win, `*`, .init = 1)

p

## ---- avg_attempts ----


avg_n_att <- reduce(diff_by_level$p_win, function(x, y) {
  x + 1 / y
}, .init = 0)

avg_n_att