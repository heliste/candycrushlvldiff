---
title: candy crush - level difficulty
author: Antti Heliste
date: '2018-07-14'
slug: candy-crush-level-difficulty
output: 
  html_document:
    toc: true
    number_sections: true 
    toc_float:
      collapsed: false
      smooth_scroll: false
    css: main.css
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#library(checkpoint)
#checkpoint("2018-07-24", checkpointLocation = tempdir())

```


As my first short project, I have chosen to study the level difficulty of [Candy Crush Saga](https://candycrushsaga.com/), a popular mobile game developed by King (part of Activision|Blizzard). The game consists of a series of levels where players need to match similar candies together in order to finish the level and to advance to the next one.

The game has more than 3000 levels and new ones are added every week. To ensure a pleasant but challenging experience for the user, both difficult and easy levels should be provided in the game. In this post, I seek to analyse the difficulty profile of one Candy Crush episode, a collection of 15 levels, and to determine whether the episode is difficult or easy.

My project is based on [a learning project at Datacamp.com](https://www.datacamp.com/projects/139), an online platform for learning data analysis skills. The project, including its tasks and data sets, has been in turn developed by Xavier Guardiola, a lead data scientist at King. 

##load script

Loading my R script from [the project repository](https://bitbucket.org/heliste/candycrushlvldiff/src).

```{r}
knitr::read_chunk(path = 'candycrush.R')
```


## the data set

Before we start, we need to load some packages that will be used in the analysis.

```{r custom_font, include = F}
```

```{r load_packages, message = F}
```

Our dataset contains one week of data from a sample of players who played Candy Crush in 2014. The data were collected from a single episode consisting of 15 levels. The set has the following variables:

  * **player_id**: a unique player id

  * **dt**: the date

  * **level**: the level number within the episode, from 1 to 15.

  * **num_attempts**: number of level attempts for the player on that level and date.

  * **num_success**: number of level attempts that resulted in a success/win for the player on that level and date.
  
Let's load in the dataset and take a look at the first couple of rows.

```{r load_csv}
```

## checking the data set

Now that the dataset is loaded, let's ensure that none of the `player_id`s are missing and check how many unique players are in the set and what time interval we are looking at. 

```{r check_data}
```

It seems that we have `r nUnique` unique `player_id`s, of which `r nNa` is missing, and the dates range between `r rang`.

Let's calculate the total number of attempts and players and the average attempts per player for each level.

###group by date and summarise
```{r calc_attempts}
```

We can plot these data to check if any significant differences exist.

###attempts per player
```{r plot_attempts}
```

As we see, especially the level 15 has received a lot of attempts per player, probably because of that level's difficulty. Next we will analyse those difficulties more closely.

##computing level difficulty

A Candy Crush episode consists of a mix of easier and tougher levels. The number of attempts required to pass a level depends on the skill and luck of the player. Moreover, the assumption is that difficult levels should require more attempts on average than the easier ones. In other words, the probability of passing should be lower.

The probability of passing a level can be simplisticly modelled as a Bernoulli process with a binary outcome (win or lose). This probability can be estimated for each level as:

$$  p_{win} = \frac{\sum{wins}}{\sum{attempts}}  $$
For example, if a level has been attempted 20 times and only 2 of those attempts ended up in passing the level, 
the probability of winning is calculated as $p_{win} = 2 / 20 = 10$%.

Now, let's compute the difficulty $p_{win}$ separately for each of the 15 levels.

###group by level
```{r calc_pwin}
```

##plotting difficulty profile

Now we can plot the probability of winning over the 15 levels. We call this plot the difficulty profile of the episode.

```{r plot_diffprof}
```

##spotting hard levels

Defining whether a level is difficult or not is subjective. To keep things simple, we define the threshold $p_{win}$ of a difficult level to be 10%. We can now add a line to the graph to spot the hard levels.

```{r draw_line}
```

As we can see, there are few levels that seem to have been very difficult for the users. Those levels also seem to be ones that received the most attempts per player, which isn't surprising. If we had more knowledge about the levels, we could possible determine why they are more difficult than the others.

##computing uncertainty

To measure uncertainty in our probabilities, we can calculate the standard error, $\sigma_{error}$, using the following formula:

$$ \sigma_{error} 	\approx \frac{\sigma_{sample}}{\sqrt{n}} $$

Here $n$ is the number of datapoints and $\sigma_{sample}$ is the sample standard deviation. For a Bernoulli process, the sample standard deviation is:

$$ \sigma_{sample} = \sqrt{p_{win}(1-p_{win}) } $$

Thus, we can calculate the standard error like this:

$$ \sigma_{error} 	\approx \sqrt{ \frac{p_{win}(1-p_{win})}{n}} $$
Applying this data to our data frame

```{r calc_error}
```

##showing uncertainty

Now that we have a measure for uncertainty, we can add error bars to our plot to show this uncertainty. The error bars show the range from $p_{win} - \sigma_{error}$ to $p_{win} + \sigma_{error}$.

```{r plot_uncert}
```

##probability of a passing all the levels

A level designer might be interested in the question: "How likely is it that a player will complete the episode without losing a single time?" Let's calculate this using the estimated level difficulties:

```{r prop_win_all}
```

The probability of passing all the levels on first attempt is very low - only `r p`. Thus a level designer should not worry that the game is too easy. 

To confirm this, we can also calculate the average number of attempts needed to pass the episode. Since $p_{win}$ is the chance to pass a level, then $1/p_{win}$ is the average number of attempts needed to pass a level. In total, this becomes:

```{r avg_attempts}
```

On average, a player needs `r round(avg_n_att)` attempts to pass the whole episode. That's a lot of playing!