---
title: candy crush - level difficulty
author: Antti Heliste
date: '2018-07-14'
slug: candy-crush-level-difficulty
output: 
  html_document:
    toc: true
    number_sections: true 
    toc_float:
      collapsed: false
      smooth_scroll: false
    css: main.css
---

<script src="/rmarkdown-libs/htmlwidgets/htmlwidgets.js"></script>
<script src="/rmarkdown-libs/plotly-binding/plotly.js"></script>
<script src="/rmarkdown-libs/typedarray/typedarray.min.js"></script>
<script src="/rmarkdown-libs/jquery/jquery.min.js"></script>
<link href="/rmarkdown-libs/crosstalk/css/crosstalk.css" rel="stylesheet" />
<script src="/rmarkdown-libs/crosstalk/js/crosstalk.min.js"></script>
<link href="/rmarkdown-libs/plotly-htmlwidgets-css/plotly-htmlwidgets.css" rel="stylesheet" />
<script src="/rmarkdown-libs/plotly-main/plotly-latest.min.js"></script>


<p>As my first short project, I have chosen to study the level difficulty of <a href="https://candycrushsaga.com/">Candy Crush Saga</a>, a popular mobile game developed by King (part of Activision|Blizzard). The game consists of a series of levels where players need to match similar candies together in order to finish the level and to advance to the next one.</p>
<p>The game has more than 3000 levels and new ones are added every week. To ensure a pleasant but challenging experience for the user, both difficult and easy levels should be provided in the game. In this post, I seek to analyse the difficulty profile of one Candy Crush episode, a collection of 15 levels, and to determine whether the episode is difficult or easy.</p>
<p>My project is based on <a href="https://www.datacamp.com/projects/139">a learning project at Datacamp.com</a>, an online platform for learning data analysis skills. The project, including its tasks and data sets, has been in turn developed by Xavier Guardiola, a lead data scientist at King.</p>
<div id="load-script" class="section level2">
<h2>load script</h2>
<p>Loading my R script from <a href="https://bitbucket.org/heliste/candycrushlvldiff/src">the project repository</a>.</p>
<pre class="r"><code>knitr::read_chunk(path = &#39;candycrush.R&#39;)</code></pre>
</div>
<div id="the-data-set" class="section level2">
<h2>the data set</h2>
<p>Before we start, we need to load some packages that will be used in the analysis.</p>
<pre class="r"><code>library(dplyr)
library(readr)
library(plotly)
library(purrr)</code></pre>
<p>Our dataset contains one week of data from a sample of players who played Candy Crush in 2014. The data were collected from a single episode consisting of 15 levels. The set has the following variables:</p>
<ul>
<li><p><strong>player_id</strong>: a unique player id</p></li>
<li><p><strong>dt</strong>: the date</p></li>
<li><p><strong>level</strong>: the level number within the episode, from 1 to 15.</p></li>
<li><p><strong>num_attempts</strong>: number of level attempts for the player on that level and date.</p></li>
<li><p><strong>num_success</strong>: number of level attempts that resulted in a success/win for the player on that level and date.</p></li>
</ul>
<p>Let’s load in the dataset and take a look at the first couple of rows.</p>
<pre class="r"><code>data &lt;- read_csv(&quot;candy_crush.csv&quot;)</code></pre>
<pre><code>## Parsed with column specification:
## cols(
##   player_id = col_character(),
##   dt = col_date(format = &quot;&quot;),
##   level = col_integer(),
##   num_attempts = col_integer(),
##   num_success = col_integer()
## )</code></pre>
<pre class="r"><code>head(data)</code></pre>
<pre><code>## # A tibble: 6 x 5
##   player_id                      dt         level num_attempts num_success
##   &lt;chr&gt;                          &lt;date&gt;     &lt;int&gt;        &lt;int&gt;       &lt;int&gt;
## 1 6dd5af4c7228fa353d505767143f5~ 2014-01-04     4            3           1
## 2 c7ec97c39349ab7e4d39b4f74062e~ 2014-01-01     8            4           1
## 3 c7ec97c39349ab7e4d39b4f74062e~ 2014-01-05    12            6           0
## 4 a32c5e9700ed356dc8dd5bb3230c5~ 2014-01-03    11            1           1
## 5 a32c5e9700ed356dc8dd5bb3230c5~ 2014-01-07    15            6           0
## 6 b94d403ac4edf639442f93eeffdc7~ 2014-01-01     8            8           1</code></pre>
</div>
<div id="checking-the-data-set" class="section level2">
<h2>checking the data set</h2>
<p>Now that the dataset is loaded, let’s ensure that none of the <code>player_id</code>s are missing and check how many unique players are in the set and what time interval we are looking at.</p>
<pre class="r"><code>#Number of NA player IDs
nNa &lt;- data %&gt;%
  filter(is.na(player_id)) %&gt;%
  summarise(n())

nNa</code></pre>
<pre><code>## # A tibble: 1 x 1
##   `n()`
##   &lt;int&gt;
## 1     0</code></pre>
<pre class="r"><code>#Number of unique players
nUnique &lt;- data %&gt;%
  filter(!is.na(player_id)) %&gt;%
  summarise(n = n_distinct(player_id))

nUnique</code></pre>
<pre><code>## # A tibble: 1 x 1
##       n
##   &lt;int&gt;
## 1  6814</code></pre>
<pre class="r"><code>#Time range
rang &lt;- range(data$dt)

rang</code></pre>
<pre><code>## [1] &quot;2014-01-01&quot; &quot;2014-01-07&quot;</code></pre>
<p>It seems that we have 6814 unique <code>player_id</code>s, of which 0 is missing, and the dates range between 2014-01-01, 2014-01-07.</p>
<p>Let’s calculate the total number of attempts and players and the average attempts per player for each level.</p>
<div id="group-by-date-and-summarise" class="section level3">
<h3>group by date and summarise</h3>
<pre class="r"><code>tot_attempt &lt;- data %&gt;%
  group_by(level) %&gt;%
  summarise(
    tot_attempts = sum(num_attempts),
    tot_players = n_distinct(player_id),
    avg_attempts = tot_attempts / tot_players
  )
tot_attempt</code></pre>
<pre><code>## # A tibble: 15 x 4
##    level tot_attempts tot_players avg_attempts
##    &lt;int&gt;        &lt;int&gt;       &lt;int&gt;        &lt;dbl&gt;
##  1     1         1322         676         1.96
##  2     2         1285         680         1.89
##  3     3         1546         673         2.30
##  4     4         1893         705         2.69
##  5     5         6937        1200         5.78
##  6     6         1591         670         2.37
##  7     7         4526         966         4.69
##  8     8        15816        1839         8.60
##  9     9         8241        1216         6.78
## 10    10         3282         879         3.73
## 11    11         5575         994         5.61
## 12    12         6868        1111         6.18
## 13    13         1327         687         1.93
## 14    14         2772         844         3.28
## 15    15        30374        2836        10.7</code></pre>
<p>We can plot these data to check if any significant differences exist.</p>
</div>
<div id="attempts-per-player" class="section level3">
<h3>attempts per player</h3>
<pre class="r"><code>tot_attempt %&gt;%
  plot_ly(
    x = ~ level,
    y =  ~ avg_attempts,
    type = &quot;bar&quot;,
    name = &quot;number of attempts / player&quot;
  ) %&gt;%
  layout(
    title = &quot;attempts per player&quot;,
    yaxis = list(title = &#39;count&#39;),
    xaxis = list(title = &#39;level&#39;),
    font = cust_font
  )</code></pre>
<div id="htmlwidget-1" style="width:672px;height:480px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-1">{"x":{"visdat":{"4c78bc26cc4":["function () ","plotlyVisDat"]},"cur_data":"4c78bc26cc4","attrs":{"4c78bc26cc4":{"x":{},"y":{},"name":"number of attempts / player","alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"bar"}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"title":"attempts per player","yaxis":{"domain":[0,1],"automargin":true,"title":"count"},"xaxis":{"domain":[0,1],"automargin":true,"title":"level"},"font":{"family":"Equity Caps A","size":16},"hovermode":"closest","showlegend":false},"source":"A","config":{"modeBarButtonsToAdd":[{"name":"Collaborate","icon":{"width":1000,"ascent":500,"descent":-50,"path":"M487 375c7-10 9-23 5-36l-79-259c-3-12-11-23-22-31-11-8-22-12-35-12l-263 0c-15 0-29 5-43 15-13 10-23 23-28 37-5 13-5 25-1 37 0 0 0 3 1 7 1 5 1 8 1 11 0 2 0 4-1 6 0 3-1 5-1 6 1 2 2 4 3 6 1 2 2 4 4 6 2 3 4 5 5 7 5 7 9 16 13 26 4 10 7 19 9 26 0 2 0 5 0 9-1 4-1 6 0 8 0 2 2 5 4 8 3 3 5 5 5 7 4 6 8 15 12 26 4 11 7 19 7 26 1 1 0 4 0 9-1 4-1 7 0 8 1 2 3 5 6 8 4 4 6 6 6 7 4 5 8 13 13 24 4 11 7 20 7 28 1 1 0 4 0 7-1 3-1 6-1 7 0 2 1 4 3 6 1 1 3 4 5 6 2 3 3 5 5 6 1 2 3 5 4 9 2 3 3 7 5 10 1 3 2 6 4 10 2 4 4 7 6 9 2 3 4 5 7 7 3 2 7 3 11 3 3 0 8 0 13-1l0-1c7 2 12 2 14 2l218 0c14 0 25-5 32-16 8-10 10-23 6-37l-79-259c-7-22-13-37-20-43-7-7-19-10-37-10l-248 0c-5 0-9-2-11-5-2-3-2-7 0-12 4-13 18-20 41-20l264 0c5 0 10 2 16 5 5 3 8 6 10 11l85 282c2 5 2 10 2 17 7-3 13-7 17-13z m-304 0c-1-3-1-5 0-7 1-1 3-2 6-2l174 0c2 0 4 1 7 2 2 2 4 4 5 7l6 18c0 3 0 5-1 7-1 1-3 2-6 2l-173 0c-3 0-5-1-8-2-2-2-4-4-4-7z m-24-73c-1-3-1-5 0-7 2-2 3-2 6-2l174 0c2 0 5 0 7 2 3 2 4 4 5 7l6 18c1 2 0 5-1 6-1 2-3 3-5 3l-174 0c-3 0-5-1-7-3-3-1-4-4-5-6z"},"click":"function(gd) { \n        // is this being viewed in RStudio?\n        if (location.search == '?viewer_pane=1') {\n          alert('To learn about plotly for collaboration, visit:\\n https://cpsievert.github.io/plotly_book/plot-ly-for-collaboration.html');\n        } else {\n          window.open('https://cpsievert.github.io/plotly_book/plot-ly-for-collaboration.html', '_blank');\n        }\n      }"}],"cloud":false},"data":[{"x":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],"y":[1.95562130177515,1.88970588235294,2.29717682020802,2.68510638297872,5.78083333333333,2.37462686567164,4.68530020703934,8.60032626427406,6.77713815789474,3.73378839590444,5.60865191146881,6.18181818181818,1.9315866084425,3.28436018957346,10.7101551480959],"name":"number of attempts / player","type":"bar","marker":{"color":"rgba(31,119,180,1)","line":{"color":"rgba(31,119,180,1)"}},"error_y":{"color":"rgba(31,119,180,1)"},"error_x":{"color":"rgba(31,119,180,1)"},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"base_url":"https://plot.ly"},"evals":["config.modeBarButtonsToAdd.0.click"],"jsHooks":[]}</script>
<p>As we see, especially the level 15 has received a lot of attempts per player, probably because of that level’s difficulty. Next we will analyse those difficulties more closely.</p>
</div>
</div>
<div id="computing-level-difficulty" class="section level2">
<h2>computing level difficulty</h2>
<p>A Candy Crush episode consists of a mix of easier and tougher levels. The number of attempts required to pass a level depends on the skill and luck of the player. Moreover, the assumption is that difficult levels should require more attempts on average than the easier ones. In other words, the probability of passing should be lower.</p>
<p>The probability of passing a level can be simplisticly modelled as a Bernoulli process with a binary outcome (win or lose). This probability can be estimated for each level as:</p>
<p><span class="math display">\[  p_{win} = \frac{\sum{wins}}{\sum{attempts}}  \]</span> For example, if a level has been attempted 20 times and only 2 of those attempts ended up in passing the level, the probability of winning is calculated as <span class="math inline">\(p_{win} = 2 / 20 = 10\)</span>%.</p>
<p>Now, let’s compute the difficulty <span class="math inline">\(p_{win}\)</span> separately for each of the 15 levels.</p>
<div id="group-by-level" class="section level3">
<h3>group by level</h3>
<pre class="r"><code>diff_by_level &lt;- data %&gt;%
  group_by(level) %&gt;%
  summarise(
    tot_wins = sum(num_success),
    tot_attempts = sum(num_attempts),
    p_win = tot_wins / tot_attempts
  )
diff_by_level</code></pre>
<pre><code>## # A tibble: 15 x 4
##    level tot_wins tot_attempts  p_win
##    &lt;int&gt;    &lt;int&gt;        &lt;int&gt;  &lt;dbl&gt;
##  1     1      818         1322 0.619 
##  2     2      666         1285 0.518 
##  3     3      662         1546 0.428 
##  4     4      705         1893 0.372 
##  5     5      634         6937 0.0914
##  6     6      668         1591 0.420 
##  7     7      614         4526 0.136 
##  8     8      641        15816 0.0405
##  9     9      670         8241 0.0813
## 10    10      617         3282 0.188 
## 11    11      603         5575 0.108 
## 12    12      659         6868 0.0960
## 13    13      686         1327 0.517 
## 14    14      777         2772 0.280 
## 15    15     1157        30374 0.0381</code></pre>
</div>
</div>
<div id="plotting-difficulty-profile" class="section level2">
<h2>plotting difficulty profile</h2>
<p>Now we can plot the probability of winning over the 15 levels. We call this plot the difficulty profile of the episode.</p>
<pre class="r"><code>diff_by_level %&gt;%
  plot_ly(
    x =  ~ level,
    y =  ~ p_win,
    type = &quot;scatter&quot;,
    mode = &quot;lines+markers&quot;,
    name = &quot;p_win&quot;
  ) %&gt;%
  layout(
    title = &quot;difficulty profile of episode&quot;,
    xaxis = list(title = &quot;level&quot;),
    yaxis = list(title = &quot;probability of winning&quot;),
    font = cust_font
  )</code></pre>
<div id="htmlwidget-2" style="width:672px;height:480px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-2">{"x":{"visdat":{"4c78392475ba":["function () ","plotlyVisDat"]},"cur_data":"4c78392475ba","attrs":{"4c78392475ba":{"x":{},"y":{},"mode":"lines+markers","name":"p_win","alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter"}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"title":"difficulty profile of episode","xaxis":{"domain":[0,1],"automargin":true,"title":"level"},"yaxis":{"domain":[0,1],"automargin":true,"title":"probability of winning"},"font":{"family":"Equity Caps A","size":16},"hovermode":"closest","showlegend":false},"source":"A","config":{"modeBarButtonsToAdd":[{"name":"Collaborate","icon":{"width":1000,"ascent":500,"descent":-50,"path":"M487 375c7-10 9-23 5-36l-79-259c-3-12-11-23-22-31-11-8-22-12-35-12l-263 0c-15 0-29 5-43 15-13 10-23 23-28 37-5 13-5 25-1 37 0 0 0 3 1 7 1 5 1 8 1 11 0 2 0 4-1 6 0 3-1 5-1 6 1 2 2 4 3 6 1 2 2 4 4 6 2 3 4 5 5 7 5 7 9 16 13 26 4 10 7 19 9 26 0 2 0 5 0 9-1 4-1 6 0 8 0 2 2 5 4 8 3 3 5 5 5 7 4 6 8 15 12 26 4 11 7 19 7 26 1 1 0 4 0 9-1 4-1 7 0 8 1 2 3 5 6 8 4 4 6 6 6 7 4 5 8 13 13 24 4 11 7 20 7 28 1 1 0 4 0 7-1 3-1 6-1 7 0 2 1 4 3 6 1 1 3 4 5 6 2 3 3 5 5 6 1 2 3 5 4 9 2 3 3 7 5 10 1 3 2 6 4 10 2 4 4 7 6 9 2 3 4 5 7 7 3 2 7 3 11 3 3 0 8 0 13-1l0-1c7 2 12 2 14 2l218 0c14 0 25-5 32-16 8-10 10-23 6-37l-79-259c-7-22-13-37-20-43-7-7-19-10-37-10l-248 0c-5 0-9-2-11-5-2-3-2-7 0-12 4-13 18-20 41-20l264 0c5 0 10 2 16 5 5 3 8 6 10 11l85 282c2 5 2 10 2 17 7-3 13-7 17-13z m-304 0c-1-3-1-5 0-7 1-1 3-2 6-2l174 0c2 0 4 1 7 2 2 2 4 4 5 7l6 18c0 3 0 5-1 7-1 1-3 2-6 2l-173 0c-3 0-5-1-8-2-2-2-4-4-4-7z m-24-73c-1-3-1-5 0-7 2-2 3-2 6-2l174 0c2 0 5 0 7 2 3 2 4 4 5 7l6 18c1 2 0 5-1 6-1 2-3 3-5 3l-174 0c-3 0-5-1-7-3-3-1-4-4-5-6z"},"click":"function(gd) { \n        // is this being viewed in RStudio?\n        if (location.search == '?viewer_pane=1') {\n          alert('To learn about plotly for collaboration, visit:\\n https://cpsievert.github.io/plotly_book/plot-ly-for-collaboration.html');\n        } else {\n          window.open('https://cpsievert.github.io/plotly_book/plot-ly-for-collaboration.html', '_blank');\n        }\n      }"}],"cloud":false},"data":[{"x":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],"y":[0.618759455370651,0.518287937743191,0.428201811125485,0.372424722662441,0.091393974340493,0.419861722187304,0.135660627485639,0.0405285786545271,0.0813008130081301,0.187995124923827,0.108161434977578,0.0959522422830518,0.516955538809344,0.28030303030303,0.0380917890300915],"mode":"lines+markers","name":"p_win","type":"scatter","marker":{"color":"rgba(31,119,180,1)","line":{"color":"rgba(31,119,180,1)"}},"error_y":{"color":"rgba(31,119,180,1)"},"error_x":{"color":"rgba(31,119,180,1)"},"line":{"color":"rgba(31,119,180,1)"},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"base_url":"https://plot.ly"},"evals":["config.modeBarButtonsToAdd.0.click"],"jsHooks":[]}</script>
</div>
<div id="spotting-hard-levels" class="section level2">
<h2>spotting hard levels</h2>
<p>Defining whether a level is difficult or not is subjective. To keep things simple, we define the threshold <span class="math inline">\(p_{win}\)</span> of a difficult level to be 10%. We can now add a line to the graph to spot the hard levels.</p>
<pre class="r"><code>diff_by_level %&gt;%
  plot_ly(
    x =  ~ level,
    y =  ~ p_win,
    type = &quot;scatter&quot;,
    mode = &quot;lines+markers&quot;,
    name = &quot;p_win&quot;
  ) %&gt;%
  add_lines(x = 0:16, y =  ~ 0.1, name = &quot;10%&quot;) %&gt;%
  layout(
    title = &quot;difficulty profile of episode&quot;,
    xaxis = list(title = &quot;level&quot;),
    yaxis = list(title = &quot;probability of winning&quot;),
    font = cust_font
  )</code></pre>
<div id="htmlwidget-3" style="width:672px;height:480px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-3">{"x":{"visdat":{"4c78732c24e0":["function () ","plotlyVisDat"]},"cur_data":"4c78732c24e0","attrs":{"4c78732c24e0":{"x":{},"y":{},"mode":"lines+markers","name":"p_win","alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter"},"4c78732c24e0.1":{"x":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],"y":{},"mode":"lines","name":"10%","alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter","inherit":true}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"title":"difficulty profile of episode","xaxis":{"domain":[0,1],"automargin":true,"title":"level"},"yaxis":{"domain":[0,1],"automargin":true,"title":"probability of winning"},"font":{"family":"Equity Caps A","size":16},"hovermode":"closest","showlegend":true},"source":"A","config":{"modeBarButtonsToAdd":[{"name":"Collaborate","icon":{"width":1000,"ascent":500,"descent":-50,"path":"M487 375c7-10 9-23 5-36l-79-259c-3-12-11-23-22-31-11-8-22-12-35-12l-263 0c-15 0-29 5-43 15-13 10-23 23-28 37-5 13-5 25-1 37 0 0 0 3 1 7 1 5 1 8 1 11 0 2 0 4-1 6 0 3-1 5-1 6 1 2 2 4 3 6 1 2 2 4 4 6 2 3 4 5 5 7 5 7 9 16 13 26 4 10 7 19 9 26 0 2 0 5 0 9-1 4-1 6 0 8 0 2 2 5 4 8 3 3 5 5 5 7 4 6 8 15 12 26 4 11 7 19 7 26 1 1 0 4 0 9-1 4-1 7 0 8 1 2 3 5 6 8 4 4 6 6 6 7 4 5 8 13 13 24 4 11 7 20 7 28 1 1 0 4 0 7-1 3-1 6-1 7 0 2 1 4 3 6 1 1 3 4 5 6 2 3 3 5 5 6 1 2 3 5 4 9 2 3 3 7 5 10 1 3 2 6 4 10 2 4 4 7 6 9 2 3 4 5 7 7 3 2 7 3 11 3 3 0 8 0 13-1l0-1c7 2 12 2 14 2l218 0c14 0 25-5 32-16 8-10 10-23 6-37l-79-259c-7-22-13-37-20-43-7-7-19-10-37-10l-248 0c-5 0-9-2-11-5-2-3-2-7 0-12 4-13 18-20 41-20l264 0c5 0 10 2 16 5 5 3 8 6 10 11l85 282c2 5 2 10 2 17 7-3 13-7 17-13z m-304 0c-1-3-1-5 0-7 1-1 3-2 6-2l174 0c2 0 4 1 7 2 2 2 4 4 5 7l6 18c0 3 0 5-1 7-1 1-3 2-6 2l-173 0c-3 0-5-1-8-2-2-2-4-4-4-7z m-24-73c-1-3-1-5 0-7 2-2 3-2 6-2l174 0c2 0 5 0 7 2 3 2 4 4 5 7l6 18c1 2 0 5-1 6-1 2-3 3-5 3l-174 0c-3 0-5-1-7-3-3-1-4-4-5-6z"},"click":"function(gd) { \n        // is this being viewed in RStudio?\n        if (location.search == '?viewer_pane=1') {\n          alert('To learn about plotly for collaboration, visit:\\n https://cpsievert.github.io/plotly_book/plot-ly-for-collaboration.html');\n        } else {\n          window.open('https://cpsievert.github.io/plotly_book/plot-ly-for-collaboration.html', '_blank');\n        }\n      }"}],"cloud":false},"data":[{"x":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],"y":[0.618759455370651,0.518287937743191,0.428201811125485,0.372424722662441,0.091393974340493,0.419861722187304,0.135660627485639,0.0405285786545271,0.0813008130081301,0.187995124923827,0.108161434977578,0.0959522422830518,0.516955538809344,0.28030303030303,0.0380917890300915],"mode":"lines+markers","name":"p_win","type":"scatter","marker":{"color":"rgba(31,119,180,1)","line":{"color":"rgba(31,119,180,1)"}},"error_y":{"color":"rgba(31,119,180,1)"},"error_x":{"color":"rgba(31,119,180,1)"},"line":{"color":"rgba(31,119,180,1)"},"xaxis":"x","yaxis":"y","frame":null},{"x":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],"y":[0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1],"mode":"lines","name":"10%","type":"scatter","marker":{"color":"rgba(255,127,14,1)","line":{"color":"rgba(255,127,14,1)"}},"error_y":{"color":"rgba(255,127,14,1)"},"error_x":{"color":"rgba(255,127,14,1)"},"line":{"color":"rgba(255,127,14,1)"},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"base_url":"https://plot.ly"},"evals":["config.modeBarButtonsToAdd.0.click"],"jsHooks":[]}</script>
<p>As we can see, there are few levels that seem to have been very difficult for the users. Those levels also seem to be ones that received the most attempts per player, which isn’t surprising. If we had more knowledge about the levels, we could possible determine why they are more difficult than the others.</p>
</div>
<div id="computing-uncertainty" class="section level2">
<h2>computing uncertainty</h2>
<p>To measure uncertainty in our probabilities, we can calculate the standard error, <span class="math inline">\(\sigma_{error}\)</span>, using the following formula:</p>
<p><span class="math display">\[ \sigma_{error}   \approx \frac{\sigma_{sample}}{\sqrt{n}} \]</span></p>
<p>Here <span class="math inline">\(n\)</span> is the number of datapoints and <span class="math inline">\(\sigma_{sample}\)</span> is the sample standard deviation. For a Bernoulli process, the sample standard deviation is:</p>
<p><span class="math display">\[ \sigma_{sample} = \sqrt{p_{win}(1-p_{win}) } \]</span></p>
<p>Thus, we can calculate the standard error like this:</p>
<p><span class="math display">\[ \sigma_{error}   \approx \sqrt{ \frac{p_{win}(1-p_{win})}{n}} \]</span> Applying this data to our data frame</p>
<pre class="r"><code>diff_by_level &lt;- diff_by_level %&gt;%
  mutate(error = sqrt(p_win * (1 - p_win) / tot_attempts))

diff_by_level</code></pre>
<pre><code>## # A tibble: 15 x 5
##    level tot_wins tot_attempts  p_win   error
##    &lt;int&gt;    &lt;int&gt;        &lt;int&gt;  &lt;dbl&gt;   &lt;dbl&gt;
##  1     1      818         1322 0.619  0.0134 
##  2     2      666         1285 0.518  0.0139 
##  3     3      662         1546 0.428  0.0126 
##  4     4      705         1893 0.372  0.0111 
##  5     5      634         6937 0.0914 0.00346
##  6     6      668         1591 0.420  0.0124 
##  7     7      614         4526 0.136  0.00509
##  8     8      641        15816 0.0405 0.00157
##  9     9      670         8241 0.0813 0.00301
## 10    10      617         3282 0.188  0.00682
## 11    11      603         5575 0.108  0.00416
## 12    12      659         6868 0.0960 0.00355
## 13    13      686         1327 0.517  0.0137 
## 14    14      777         2772 0.280  0.00853
## 15    15     1157        30374 0.0381 0.00110</code></pre>
</div>
<div id="showing-uncertainty" class="section level2">
<h2>showing uncertainty</h2>
<p>Now that we have a measure for uncertainty, we can add error bars to our plot to show this uncertainty. The error bars show the range from <span class="math inline">\(p_{win} - \sigma_{error}\)</span> to <span class="math inline">\(p_{win} + \sigma_{error}\)</span>.</p>
<pre class="r"><code>diff_by_level %&gt;%
  plot_ly(
    x =  ~ level,
    y =  ~ p_win,
    type = &quot;scatter&quot;,
    mode = &quot;lines+markers&quot;,
    name = &quot;p_win&quot;,
    error_y =  ~ list(value = error)
  ) %&gt;%
  add_lines(x = 0:16, y =  ~ 0.1, error_y = list(visible = F), name = &quot;10%&quot;) %&gt;%
  layout(
    title = &quot;difficulty profile of episode&quot;,
    xaxis = list(title = &quot;level&quot;),
    yaxis = list(title = &quot;probability of winning&quot;),
    font = cust_font
  )</code></pre>
<div id="htmlwidget-4" style="width:672px;height:480px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-4">{"x":{"visdat":{"4c78741c69be":["function () ","plotlyVisDat"]},"cur_data":"4c78741c69be","attrs":{"4c78741c69be":{"x":{},"y":{},"mode":"lines+markers","error_y":{},"name":"p_win","alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter"},"4c78741c69be.1":{"x":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],"y":{},"mode":"lines","error_y":{"visible":false},"name":"10%","alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter","inherit":true}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"title":"difficulty profile of episode","xaxis":{"domain":[0,1],"automargin":true,"title":"level"},"yaxis":{"domain":[0,1],"automargin":true,"title":"probability of winning"},"font":{"family":"Equity Caps A","size":16},"hovermode":"closest","showlegend":true},"source":"A","config":{"modeBarButtonsToAdd":[{"name":"Collaborate","icon":{"width":1000,"ascent":500,"descent":-50,"path":"M487 375c7-10 9-23 5-36l-79-259c-3-12-11-23-22-31-11-8-22-12-35-12l-263 0c-15 0-29 5-43 15-13 10-23 23-28 37-5 13-5 25-1 37 0 0 0 3 1 7 1 5 1 8 1 11 0 2 0 4-1 6 0 3-1 5-1 6 1 2 2 4 3 6 1 2 2 4 4 6 2 3 4 5 5 7 5 7 9 16 13 26 4 10 7 19 9 26 0 2 0 5 0 9-1 4-1 6 0 8 0 2 2 5 4 8 3 3 5 5 5 7 4 6 8 15 12 26 4 11 7 19 7 26 1 1 0 4 0 9-1 4-1 7 0 8 1 2 3 5 6 8 4 4 6 6 6 7 4 5 8 13 13 24 4 11 7 20 7 28 1 1 0 4 0 7-1 3-1 6-1 7 0 2 1 4 3 6 1 1 3 4 5 6 2 3 3 5 5 6 1 2 3 5 4 9 2 3 3 7 5 10 1 3 2 6 4 10 2 4 4 7 6 9 2 3 4 5 7 7 3 2 7 3 11 3 3 0 8 0 13-1l0-1c7 2 12 2 14 2l218 0c14 0 25-5 32-16 8-10 10-23 6-37l-79-259c-7-22-13-37-20-43-7-7-19-10-37-10l-248 0c-5 0-9-2-11-5-2-3-2-7 0-12 4-13 18-20 41-20l264 0c5 0 10 2 16 5 5 3 8 6 10 11l85 282c2 5 2 10 2 17 7-3 13-7 17-13z m-304 0c-1-3-1-5 0-7 1-1 3-2 6-2l174 0c2 0 4 1 7 2 2 2 4 4 5 7l6 18c0 3 0 5-1 7-1 1-3 2-6 2l-173 0c-3 0-5-1-8-2-2-2-4-4-4-7z m-24-73c-1-3-1-5 0-7 2-2 3-2 6-2l174 0c2 0 5 0 7 2 3 2 4 4 5 7l6 18c1 2 0 5-1 6-1 2-3 3-5 3l-174 0c-3 0-5-1-7-3-3-1-4-4-5-6z"},"click":"function(gd) { \n        // is this being viewed in RStudio?\n        if (location.search == '?viewer_pane=1') {\n          alert('To learn about plotly for collaboration, visit:\\n https://cpsievert.github.io/plotly_book/plot-ly-for-collaboration.html');\n        } else {\n          window.open('https://cpsievert.github.io/plotly_book/plot-ly-for-collaboration.html', '_blank');\n        }\n      }"}],"cloud":false},"data":[{"x":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],"y":[0.618759455370651,0.518287937743191,0.428201811125485,0.372424722662441,0.091393974340493,0.419861722187304,0.135660627485639,0.0405285786545271,0.0813008130081301,0.187995124923827,0.108161434977578,0.0959522422830518,0.516955538809344,0.28030303030303,0.0380917890300915],"mode":"lines+markers","error_y":{"color":"rgba(31,119,180,1)","value":[0.0133581014463577,0.0139388757741351,0.0125846427919404,0.0111116066208675,0.00345987764552507,0.0123732506951513,0.00508992970137773,0.00156800838286781,0.00301053827621856,0.00681998349496783,0.00415965113579375,0.0035539237645228,0.0137178069991115,0.00853084636179892,0.00109832661284108]},"name":"p_win","type":"scatter","marker":{"color":"rgba(31,119,180,1)","line":{"color":"rgba(31,119,180,1)"}},"error_x":{"color":"rgba(31,119,180,1)"},"line":{"color":"rgba(31,119,180,1)"},"xaxis":"x","yaxis":"y","frame":null},{"x":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],"y":[0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1],"mode":"lines","error_y":{"color":"rgba(255,127,14,1)","visible":false},"name":"10%","type":"scatter","marker":{"color":"rgba(255,127,14,1)","line":{"color":"rgba(255,127,14,1)"}},"error_x":{"color":"rgba(255,127,14,1)"},"line":{"color":"rgba(255,127,14,1)"},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"base_url":"https://plot.ly"},"evals":["config.modeBarButtonsToAdd.0.click"],"jsHooks":[]}</script>
</div>
<div id="probability-of-a-passing-all-the-levels" class="section level2">
<h2>probability of a passing all the levels</h2>
<p>A level designer might be interested in the question: “How likely is it that a player will complete the episode without losing a single time?” Let’s calculate this using the estimated level difficulties:</p>
<pre class="r"><code>p &lt;-  reduce(diff_by_level$p_win, `*`, .init = 1)

p</code></pre>
<pre><code>## [1] 9.447141e-12</code></pre>
<p>The probability of passing all the levels on first attempt is very low - only 9.447140910^{-12}. Thus a level designer should not worry that the game is too easy.</p>
<p>To confirm this, we can also calculate the average number of attempts needed to pass the episode. Since <span class="math inline">\(p_{win}\)</span> is the chance to pass a level, then <span class="math inline">\(1/p_{win}\)</span> is the average number of attempts needed to pass a level. In total, this becomes:</p>
<pre class="r"><code>avg_n_att &lt;- reduce(diff_by_level$p_win, function(x, y) {
  x + 1 / y
}, .init = 0)

avg_n_att</code></pre>
<pre><code>## [1] 122.9756</code></pre>
<p>On average, a player needs 123 attempts to pass the whole episode. That’s a lot of playing!</p>
</div>
